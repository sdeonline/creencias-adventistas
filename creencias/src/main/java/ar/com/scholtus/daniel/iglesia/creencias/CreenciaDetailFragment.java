package ar.com.scholtus.daniel.iglesia.creencias;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * A fragment representing a single Creencia detail screen.
 * This fragment is either contained in a {@link CreenciaListActivity}
 * in two-pane mode (on tablets) or a {@link CreenciaDetailActivity}
 * on handsets.
 */
public class CreenciaDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private String mItem;
    private int mId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CreenciaDetailFragment() {
    }

    public static CreenciaDetailFragment newInstance(int position) {

        CreenciaDetailFragment fragment = new CreenciaDetailFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(CreenciaDetailFragment.ARG_ITEM_ID, position);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(ARG_ITEM_ID)) {
            mId = savedInstanceState.getInt(ARG_ITEM_ID);
        } else {
            mId = getArguments().getInt(ARG_ITEM_ID, 0);
        }

        mItem = "<html><body>";
        mItem += getActivity().getResources().getTextArray(R.array.descriptions)[mId].toString().replace("\n", "<br />");
        mItem += "<br /> <br />";
        mItem += getActivity().getResources().getTextArray(R.array.verses)[mId].toString().replace("\n", "<br />");
        mItem += "</body></html>";
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARG_ITEM_ID, mId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_creencia_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            WebView webView = (WebView) rootView.findViewById(R.id.creencia_detail);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(true);
            webView.getSettings().setDefaultTextEncodingName("utf-8");
            webView.loadDataWithBaseURL(null, mItem, "text/html", "UTF-8", null);
        }

        return rootView;
    }
}
