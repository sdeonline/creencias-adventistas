package ar.com.scholtus.daniel.iglesia.creencias;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

/**
 * An activity representing a single Creencia detail screen. This
 * activity is only used on handset devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link CreenciaListActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing
 * more than a {@link CreenciaDetailFragment}.
 */
public class CreenciaDetailActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Aplicamos el layout correspondiente
        setContentView(R.layout.activity_creencia_detail);

        // Mostrar el botón para volver al inicio
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Ponemos el título correspondiente (esto no hará falta una vez terminado)
        setTitle(getResources().getStringArray(R.array.titles)[getIntent().getIntExtra(CreenciaDetailFragment.ARG_ITEM_ID, 0)]);

        // Obtenemos el paginador
        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        // Le cargamos el adaptador con los datos
        pager.setAdapter(new CreenciaPagerAdapter(getSupportFragmentManager(), this));

        // Escuchamos cuando cambie de página para actualizar el título
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // Actualizamos el título
            @Override
            public void onPageSelected(int i) {
                setTitle(getResources().getStringArray(R.array.titles)[i]);
            }

            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        Bundle bundle;
        if (savedInstanceState != null) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Se presionó el botón del icono de la aplicación
            case android.R.id.home:
                // Volvemos a la lista
                NavUtils.navigateUpTo(this, new Intent(this, CreenciaListActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
