package ar.com.scholtus.daniel.iglesia.creencias;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * An activity representing a list of Creencias. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link CreenciaDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link CreenciaListFragment} and the item details
 * (if present) is a {@link CreenciaDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link CreenciaListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class CreenciaListActivity extends FragmentActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    public static final String ARG_ITEM_ID = "item_id";
    private boolean mTwoPane;
    private DrawerLayout mDrawerLayout;
    private ListView mListView;
    private ActionBarDrawerToggle mDrawerToggle;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creencia_list);

        // Registramos la lista
        mListView = (ListView) findViewById(R.id.creencia_list);

        // Cargar elementos de la lista
        mListView.setAdapter(new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                getResources().getStringArray(
                        R.array.titles)));

        // Escuchar clicks de la lista
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onItemSelected(i);
            }
        });

        // Marcamos si estamos usando dos paneles
        if (findViewById(R.id.layout_twopane) != null) {

            mTwoPane = true;

        } else {

            mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(new CreenciaPagerAdapter(getSupportFragmentManager(),this));
            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i2) {
                    setTitle(getResources().getStringArray(R.array.titles)[i]);
                    mListView.setItemChecked(i,true);
                }

                @Override
                public void onPageSelected(int i) {}

                @Override
                public void onPageScrollStateChanged(int i) {}
            });

            // Registrar el panel de navegación.
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            // Convertir el botón de apicación en switch para el panel
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

                /** Llamado cuando el panel termina de cerrarse. */
                public void onDrawerClosed(View view) {

                    // Restaurar el título
                    setTitle(getResources().getStringArray(R.array.titles)[mListView.getCheckedItemPosition()]);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }

                /** Llamado cuando el panel termina de abrirse. */
                public void onDrawerOpened(View drawerView) {

                    // Poner de título el nombre de la aplicación
                    setTitle(getResources().getString(R.string.app_name));
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            };

            // Registrar el botón creado como switch del panel.
            mDrawerLayout.setDrawerListener(mDrawerToggle);

            // Activar botón de aplicación
            getActionBar().setDisplayHomeAsUpEnabled(true);
            // getActionBar().setHomeButtonEnabled(true);

        }

        // Si volvemos de un giro de pantalla, recuperar el título
        if (savedInstanceState != null) {

            setTitle(getResources().getStringArray(R.array.titles)[savedInstanceState.getInt(ARG_ITEM_ID, 0)]);

        }
        // Sino, seleccionar el primer elemento
        else {

            // Simulamos click en la primera posición
            this.onItemSelected(0);

            if (!mTwoPane) {

                // Abrir el panel de navegación al iniciar en modo teléfono
                mDrawerLayout.openDrawer(mListView);
                setTitle(getResources().getString(R.string.app_name));
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARG_ITEM_ID, mListView.getCheckedItemPosition());
    }

    public void onItemSelected(int id) {

        // Marcar la posición en la lista
        mListView.setItemChecked(id, true);

        // Actualizar el título.
        setTitle(getResources().getStringArray(R.array.titles)[id]);

        if (mTwoPane) {

            // Reemplazamos el contenido con el nuevo fragmento
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.creencia_detail_container, CreenciaDetailFragment.newInstance(id))
                    .commit();

        } else {

            // Movemos el paginador
            mViewPager.setCurrentItem(id);

            // Cerrar lista de navegación
            mDrawerLayout.closeDrawer(mListView);

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (!mTwoPane) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!mTwoPane) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (!mTwoPane) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
}
