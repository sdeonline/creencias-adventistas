package ar.com.scholtus.daniel.iglesia.creencias;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by daniel on 21/07/13.
 */
public class CreenciaPagerAdapter extends FragmentPagerAdapter {

    Context mContext;

    public CreenciaPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        return CreenciaDetailFragment.newInstance(i);
    }

    @Override
    public int getCount() {
        return mContext.getResources().getStringArray(R.array.titles).length;
    }

}
